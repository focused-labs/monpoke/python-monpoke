from io import StringIO
from main import CommandReader, Game, Monpoke, Team
import unittest
from unittest.mock import patch


class TestMonpoke(unittest.TestCase):
    def setUp(self):
        self.file_reader = CommandReader()
        self.team_1 = Team("Rover")

    def test_read_from_given_txt_file(self):
        test_arg = "testinput.txt"
        monpoke_string = self.file_reader.read_txt_file(test_arg)
        self.assertGreater(len(monpoke_string), 0)

    def test_read_by_line(self):
        test_str_input = """CREATE Rocket Meekachu 3 1
            CREATE Rocket Rastly 5 6"""
        monpoke_str_parsed = self.file_reader.parse_txt_file(test_str_input)
        self.assertEqual(len(monpoke_str_parsed), 2)

    def test_throws_argument_error_empty_file(self):
        test_str_input = ""
        with self.assertRaises(ValueError) as cm:
            self.file_reader.parse_txt_file(test_str_input)
        self.assertIsInstance(cm.exception, ValueError)

    def test_game_runs_gameplay(self):
        with patch('sys.stdout', new=StringIO()) as output:
            game = Game(["Rover Katy 5 1"])
            game.play()
            self.assertTrue(output.getvalue())

    def test_game_has_both_teams(self):
        game = Game()
        game.add_team("Rover")
        game.add_team("Shuttle")
        self.assertTrue(game.has_both_teams())

    def test_game_creates_teams(self):
        game = Game()
        game.add_team(Team("Rover"))
        self.assertEquals(len(game.teams), 1)

    def test_game_create_team(self):
        game = Game(["CREATE Rover Katy 5 1"])
        game.play()
        self.assertIn("Katy has been assigned to team Rover", game.output)

    def test_game_choose_monpoke(self):
        game = Game(["CREATE Rover Katy 5 1", "CREATE Shuttle Noa 5 1", 'Katy ICHOOSEYOU'])
        game.play()
        self.assertIn("Katy enters the battle", game.output)

    def test_game_attack(self):
        game = Game(
            ["CREATE Rover Katy 5 1", "CREATE Shuttle Noa 5 1", 'Katy ICHOOSEYOU', "Noa ICHOOSEYOU", "Katy ATTACK"])
        game.play()
        self.assertIn("Katy attacked Noa for 1 damage", game.output)

    def test_monpoke_defeated(self):
        game = Game(
            ["CREATE Rover Katy 5 5", "CREATE Shuttle Noa 5 1", 'Katy ICHOOSEYOU', "Noa ICHOOSEYOU", "Katy ATTACK"])
        game.play()
        self.assertIn("Noa has been defeated!", game.output)

    def test_monpoke_game_over(self):
        game = Game(
            ["CREATE Rover Katy 5 5", "CREATE Shuttle Noa 5 1", 'Katy ICHOOSEYOU', "Noa ICHOOSEYOU", "Katy ATTACK"])
        game.play()
        self.assertIn("Team Rover wins!", game.output)

    def test_team_creation(self):
        self.assertEqual(len(self.team_1.monpokes), 0)
        self.assertFalse(self.team_1.defeated)

    def test_team_add_monpoke(self):
        self.team_1.add("Katy", 5, 1)
        self.assertEquals(len(self.team_1.monpokes), 1)

    def test_team_monpoke_names_must_be_unique(self):
        self.team_1.add("Katy", 5, 1)
        with self.assertRaises(ValueError) as cm:
            self.team_1.add('Katy', 5, 1)
        self.assertIsInstance(cm.exception, ValueError)

    def test_team_select_monpoke(self):
        self.team_1.add("Katy", 5, 1)
        self.team_1.select("Katy")
        self.assertEqual(self.team_1.chosen.name, "Katy")

    def test_team_select_monpoke_raises_error_if_not_there(self):
        with self.assertRaises(ValueError) as cm:
            self.team_1.select('Katy')
        self.assertIsInstance(cm.exception, ValueError)

    def test_team_select_monpoke_raises_error_if_defeated(self):
        self.team_1.add("Katy", 0, 1)
        with self.assertRaises(ValueError) as cm:
            self.team_1.select('Katy')
        self.assertIsInstance(cm.exception, ValueError)

    def test_team_is_defeated_if_all_monpoke_hp_is_0(self):
        self.team_1.add("Katy", 5, 1)
        self.team_1.monpokes[0].hp = 0
        self.team_1.check_defeated()
        self.assertTrue(self.team_1.defeated)

    def test_monpoke_get_hit(self):
        monpoke = Monpoke('Katy', 5, 1)
        monpoke.get_hit(1)
        self.assertEqual(monpoke.hp, 4)

    def test_monpoke_is_defeated(self):
        monpoke = Monpoke("Katy", 1, 1)
        monpoke.get_hit(1)
        self.assertTrue(monpoke.is_defeated)


if __name__ == '__main__':
    unittest.main()
