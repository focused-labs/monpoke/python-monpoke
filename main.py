import os
import sys


class CommandReader:
    def read_txt_file(self, file_name):
        txt_file = os.path.join(os.path.dirname(__file__), file_name)
        commands = open(txt_file, "r")
        return commands.read()

    def parse_txt_file(self, input_str):
        parsed_str = input_str.split("\n")
        if not parsed_str[0]:
            raise ValueError
        return parsed_str


class Game:
    def __init__(self, moves=[""]):
        self.moves = moves
        self.teams = []
        self.output = ""

    def play(self):
        for arg in self.moves:
            args_split = arg.split(" ")
            if args_split[0] == 'CREATE':
                team_name = args_split[1]
                mp_name = args_split[2]
                if not self.has_both_teams():
                    self.add_team(team_name)
                team_by_name = [i for i in self.teams if i.name == team_name][0]
                team_by_name.add(mp_name, args_split[3], args_split[4])
                self.output += f'{mp_name} has been assigned to team {team_name}'
            if args_split[1] == 'ICHOOSEYOU':
                monpoke_name = args_split[0]
                team_by_monpoke_name = [i for i in self.teams if i.has_monpoke(monpoke_name)][0]
                if not team_by_monpoke_name:
                    raise ValueError
                team_by_monpoke_name.select(monpoke_name)
                self.output += f'{monpoke_name} enters the battle'
            if args_split[1] == "ATTACK":
                monpoke_name = args_split[0]
                attacking_team = [i for i in self.teams if i.has_monpoke(monpoke_name)][0]
                attacked_team = [i for i in self.teams if not i.has_monpoke(monpoke_name)][0]
                if not (attacking_team or attacked_team):
                    raise ValueError
                attacked_team.chosen.get_hit(attacking_team.chosen.ap)
                self.reset_teams(attacking_team, attacked_team)
                self.output += f'{monpoke_name} attacked {attacked_team.chosen.name} for {attacking_team.chosen.ap} damage!'
                attacked_team.check_defeated()
                if attacked_team.chosen.is_defeated():
                    self.output += f' {attacked_team.chosen.name} has been defeated!'
                if attacked_team.defeated:
                    self.output += f'\nTeam {attacking_team.name} wins!'
                    break
            self.output += "\n"
        print(self.output)

    def add_team(self, name):
        new_team = Team(name)
        curr_teams = [i for i in self.teams if i.name == name]
        if len(curr_teams) > 0:
            return
        self.teams.append(new_team)

    def has_both_teams(self):
        return len(self.teams) == 2

    def reset_teams(self, team1, team2):
        self.teams = []
        self.teams.append(team1)
        self.teams.append(team2)


class Team:
    def __init__(self, name):
        self.name = name
        self.monpokes = []
        self.defeated = False
        self.chosen = None

    def add(self, name, hp, ap):
        monpoke = Monpoke(name, hp, ap)
        if [mon for mon in self.monpokes if mon.name == name]:
            raise ValueError
        self.monpokes.append(monpoke)

    def select(self, name):
        monpokes = [mon for mon in self.monpokes if mon.name == name]
        if len(monpokes) < 1 or monpokes[0].is_defeated():
            raise ValueError
        self.chosen = monpokes[0]

    def check_defeated(self):
        active_monpokes = [mon for mon in self.monpokes if mon.is_defeated() == False]
        self.defeated = len(active_monpokes) < 1

    def has_monpoke(self, name):
        monpokes_found = [i for i in self.monpokes if i.name == name]
        return len(monpokes_found) > 0


class Monpoke:
    def __init__(self, name, hp, ap):
        self.name = name
        self.hp = int(hp)
        self.ap = int(ap)

    def get_hit(self, damage):
        self.hp -= damage

    def is_defeated(self):
        return self.hp <= 0


if __name__ == "__main__":
    reader = CommandReader()
    text = reader.read_txt_file(sys.argv[1])
    game = Game(reader.parse_txt_file(text))
    game.play()
